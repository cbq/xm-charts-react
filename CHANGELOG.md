## [1.1.5](https://gitee.com/cbq/xm-charts-react/compare/v1.1.4...v1.1.5) (2020-06-21)

### build

- 生成 changelog ([19dfcac](https://gitee.com/cbq/xm-charts-react/commits/19dfcac4e3d0a58a49650cb4c11cccade0f695bf))

### fix

- 优化 echarts 安装 ([2c6400f](https://gitee.com/cbq/xm-charts-react/commits/2c6400f0ce9b5557ce23f085b9853b4db9ab3559))
- 优化 echarts 安装 ([8a6197b](https://gitee.com/cbq/xm-charts-react/commits/8a6197b09cbbd27854707d3128bc9f294f64926a))

## [1.1.4](https://gitee.com/cbq/xm-charts-react/compare/v1.1.2-0...v1.1.4) (2020-06-17)

### docs

- 增加说明 ([007db68](https://gitee.com/cbq/xm-charts-react/commits/007db6869068320eda3e885b681ea629c9bb0204))

### fix

- 修复 line bar 混合的情况 ([502a5d6](https://gitee.com/cbq/xm-charts-react/commits/502a5d600f963f3d1f554a8c6972923881b6fcaf))

## [1.1.2-0](https://gitee.com/cbq/xm-charts-react/compare/v1.1.1-alpha.2...v1.1.2-0) (2020-06-12)

## [1.1.1-alpha.2](https://gitee.com/cbq/xm-charts-react/compare/v1.1.1-alpha.1...v1.1.1-alpha.2) (2020-06-12)

## [1.1.1-alpha.1](https://gitee.com/cbq/xm-charts-react/compare/v1.1.3-1...v1.1.1-alpha.1) (2020-06-12)

## [1.1.3-1](https://gitee.com/cbq/xm-charts-react/compare/v1.1.3-0...v1.1.3-1) (2020-06-12)

## [1.1.3-0](https://gitee.com/cbq/xm-charts-react/compare/v1.1.1...v1.1.3-0) (2020-06-12)

## [1.1.1](https://gitee.com/cbq/xm-charts-react/compare/v1.0.7...v1.1.1) (2020-06-11)

## [1.0.7](https://gitee.com/cbq/xm-charts-react/compare/v1.0.6...v1.0.7) (2020-06-11)

## [1.0.6](https://gitee.com/cbq/xm-charts-react/compare/v1.0.2...v1.0.6) (2020-06-11)

### docs

- 增加 readme ([2f0fdd0](https://gitee.com/cbq/xm-charts-react/commits/2f0fdd0eedbe915892f1425e4549b805f4cdc2c2))

## [1.0.2](https://gitee.com/cbq/xm-charts-react/compare/edbc3cf1459ef0bf4bc322fa65d57c58ac46070c...v1.0.2) (2020-06-09)

### fix

- module ([edbc3cf](https://gitee.com/cbq/xm-charts-react/commits/edbc3cf1459ef0bf4bc322fa65d57c58ac46070c))
