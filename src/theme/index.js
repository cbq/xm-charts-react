import { registerTheme } from 'echarts/lib/echarts'

// 主题色
const colorPalette = [
  '#1890FF',
  '#41D9C7',
  '#2FC25B',
  '#FACC14',
  '#E6965C',
  '#223273',
  '#7564CC',
  '#8543E0',
  '#5C8EE6',
  '#13C2C2',
  '#5CA3E6',
  '#3436C7',
  '#B381E6',
  '#F04864',
  '#D598D9',
]

const theme = {
  color: colorPalette,

  title: {
    textStyle: {
      fontWeight: 550,
      color: '#262a30',
      fontSize: 14,
    },
  },

  visualMap: {
    itemWidth: 15,
    color: ['#5ab1ef', '#e0ffff'],
  },

  tooltip: {
    backgroundColor: '#fff',
    textStyle: {
      color: '#333',
    },
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,.15)',
    axisPointer: {
      type: 'line',
      lineStyle: {
        color: 'rgba(0,0,0,.3)',
      },
      crossStyle: {
        color: '#008acd',
      },

      shadowStyle: {
        color: 'rgba(200,200,200,0.2)',
      },
    },
  },

  dataZoom: {
    dataBackgroundColor: 'rgba(149, 155, 163, 0.2)',
    fillerColor: 'rgba(149, 155, 163, 0.4)',
    borderRadius: 4,
    handleColor: '#008acd',
  },

  categoryAxis: {
    axisLine: {
      lineStyle: {
        color: '#ccd6eb',
      },
    },
    axisLabel: {
      color: '#333',
      // interval: 0
    },
    splitLine: {
      lineStyle: {
        color: ['#000'],
      },
    },
  },

  valueAxis: {
    axisLine: {
      lineStyle: {
        color: '#000',
      },
    },
    splitLine: {
      lineStyle: {
        color: ['#959ba3'],
      },
    },
  },
}

registerTheme('macarons', theme)
