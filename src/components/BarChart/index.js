import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import ReactEchartsCore from '../Core/EchartsReactCore'

import {
  titleTextStyle,
  lbLegend,
  lbGrid,
  lbXAxis,
  lbYAxis,
  labelStyle,
  dataZoomConfig,
} from '../../itemDefaultConfig'
import Empty from '../Empty'
import { formatNumber } from '../../utils'

const BarChart = forwardRef(
  (
    {
      config: { title, legend, grid, xAxis, yAxis, series, ...otherConfig },
      showTitle,
      rotate,
      showLabel,
      interval,
      dataZoom,
      empty: EmptyComponent,
      ...rest
    },
    ref
  ) => {
    const { isArray } = Array

    const option = {
      title: {
        ...titleTextStyle,
        show: showTitle, // title.show优先级高于showTitle
        ...title,
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      legend: {
        top: showTitle ? 27 : 'top',
        ...lbLegend,
        ...legend,
      },
      grid: {
        top: showTitle ? 66 : 36,
        ...lbGrid,
        ...grid,
      },
      dataZoom: dataZoom ? dataZoomConfig : [],
      xAxis: isArray(xAxis)
        ? xAxis.map((item) => ({
            axisLabel: {
              interval: interval ? 'auto' : 0,
              rotate: rotate || 0,
            },
            ...lbXAxis,
            ...item,
          }))
        : {
            axisLabel: {
              interval: interval ? 'auto' : 0,
              rotate: rotate || 0,
            },
            ...lbXAxis,
            ...xAxis,
          },

      yAxis: isArray(yAxis)
        ? yAxis.map((item) => ({ ...lbYAxis, ...item }))
        : {
            ...lbYAxis,
            ...yAxis,
          },

      series: series || [],
      ...otherConfig,
    }

    let targetArr = []

    option.series.forEach((item) => {
      if (showLabel || (item.label && item.label.show)) {
        item.label = {
          ...labelStyle,
          formatter({ value }) {
            // 千分位分隔符
            return formatNumber(value)
          },
          ...item.label,
        }
      }

      item.type = item.type || 'bar'
      item.barMaxWidth = item.barMaxWidth || 100
      targetArr = targetArr.concat(item.data || [])
    })

    if (!targetArr.length) {
      return EmptyComponent ? <EmptyComponent /> : <Empty />
    }

    return <ReactEchartsCore notMerge config={option} ref={ref} {...rest} />
  }
)

BarChart.propTypes = {
  theme: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  interval: PropTypes.bool,
  showTitle: PropTypes.bool,
  showLabel: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
  config: PropTypes.shape({
    xAxis: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    yAxis: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    series: PropTypes.array.isRequired,
  }).isRequired,
  rotate: PropTypes.number,
  dataZoom: PropTypes.bool, // 是否开启x轴滚动，适合于x轴数据
  opts: PropTypes.shape({
    devicePixelRatio: PropTypes.number,
    renderer: PropTypes.oneOf(['canvas', 'svg']),
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf([null, undefined, 'auto'])]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf([null, undefined, 'auto'])]),
  }),
}

BarChart.defaultProps = {
  style: {},
  classname: '',
  theme: 'macarons',
  rotate: 0,
  showTitle: false,
  interval: true,
  showLabel: false,
  dataZoom: false, // 是否开启x轴滚动
  config: {
    title: {},
    legend: {},
    toolbox: {},
    grid: {},
    xAxis: [
      {
        data: ['示意1', '示意2'],
      },
    ],
    yAxis: [{ ...lbYAxis }],
    series: [
      {
        data: [200, 600],
        type: 'bar',
        name: '柱状图',
      },
    ],
  },
}

export default BarChart
