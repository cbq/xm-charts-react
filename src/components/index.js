import { registerTheme } from 'echarts/lib/echarts'

import '../theme'
import '../dependency'

import BarChart from './BarChart'
import LineChart from './LineChart'
import PieChart from './PieChart'
import AreaChart from './AreaChart'
import WordCloud from './WordCloud'
import Chart from './Core/EchartsReactCore'

export { Chart, BarChart, PieChart, LineChart, AreaChart, WordCloud, registerTheme }
