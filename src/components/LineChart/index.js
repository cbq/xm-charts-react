import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import ReactEchartsCore from '../Core/EchartsReactCore'

import {
  titleTextStyle,
  lbLegend,
  lbGrid,
  lbXAxis,
  lbYAxis,
  labelStyle,
  dataZoomConfig,
} from '../../itemDefaultConfig'
import Empty from '../Empty'
import { formatNumber } from '../../utils'

const createTopRate = (data, isRate, i) =>
  `${i === 0 ? data.name : ''}</br> ${data.marker} ${data.seriesName} :${data.data}${
    isRate ? '%' : ''
  }`

const LineChart = forwardRef(
  (
    {
      config: { title, legend, grid, xAxis, yAxis, series, ...otherConfig },
      showTitle,
      rotate,
      showLabel,
      interval,
      dataZoom,
      isRate,
      empty: EmptyComponent,
      ...rest
    },
    ref
  ) => {
    const { isArray } = Array

    const option = {
      title: {
        ...titleTextStyle,
        show: showTitle, // title.show优先级高于showTitle
        ...title,
      },
      tooltip: {
        trigger: 'axis',
        formatter: (data) => data.map((data, i) => createTopRate(data, isRate, i)).join(''),
      },
      legend: {
        top: showTitle ? 27 : 'top',
        ...lbLegend,
        ...legend,
      },

      grid: {
        top: showTitle ? 66 : 36,
        ...lbGrid,
        ...grid,
      },
      dataZoom: dataZoom ? dataZoomConfig : [],
      xAxis: isArray(xAxis)
        ? xAxis.map((item) => ({
            axisLabel: {
              interval: interval ? 'auto' : 0,
              rotate: rotate || 0,
            },
            ...lbXAxis,
            ...item,
          }))
        : {
            axisLabel: {
              interval: interval ? 'auto' : 0,
              rotate: rotate || 0,
            },
            ...lbXAxis,
            ...xAxis,
          },

      yAxis: isArray(yAxis)
        ? yAxis.map((item) => ({
            axisLabel: { formatter: isRate ? '{value}%' : '{value}' },
            ...lbYAxis,
            ...item,
          }))
        : {
            axisLabel: { formatter: isRate ? '{value}%' : '{value}' },
            ...lbYAxis,
            ...yAxis,
          },

      series: series || [],
      ...otherConfig,
    }

    let targetArr = []

    option.series.forEach((item) => {
      if (showLabel || (item.label && item.label.show)) {
        item.label = {
          ...labelStyle,
          formatter: isRate
            ? '{c}%'
            : function ({ value }) {
                // 千分位分隔符

                return formatNumber(value)
              },
          ...item.label,
        }
      }
      item.type = item.type || 'line'
      targetArr = targetArr.concat(item.data || [])
    })

    if (!targetArr.length) {
      return EmptyComponent ? <EmptyComponent /> : <Empty />
    }

    return <ReactEchartsCore notMerge config={option} ref={ref} {...rest} />
  }
)

LineChart.propTypes = {
  theme: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  interval: PropTypes.bool,
  showTitle: PropTypes.bool,
  showLabel: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
  isRate: PropTypes.bool, // 是否带百分比
  config: PropTypes.shape({
    xAxis: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    yAxis: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    series: PropTypes.array.isRequired,
  }).isRequired,
  rotate: PropTypes.number,
  dataZoom: PropTypes.bool, // 是否开启x轴滚动，适合于x轴数据
  opts: PropTypes.shape({
    devicePixelRatio: PropTypes.number,
    renderer: PropTypes.oneOf(['canvas', 'svg']),
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf([null, undefined, 'auto'])]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf([null, undefined, 'auto'])]),
  }),
}

LineChart.defaultProps = {
  style: {},
  classname: '',
  theme: 'macarons',
  rotate: 0,
  isRate: false,
  showTitle: false,
  interval: true,
  showLabel: false,
  dataZoom: false,
  config: {
    title: {},
    legend: {},
    toolbox: {},
    grid: {},
    xAxis: [
      {
        data: ['示意1', '示意2'],
      },
    ],
    yAxis: [{ ...lbYAxis }],
    series: [
      {
        data: [200, 600],
        type: 'line',
        name: '折线图',
      },
    ],
  },
}

export default LineChart
