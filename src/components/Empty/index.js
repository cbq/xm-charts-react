import React from 'react'

import styles from './index.scss'

const NoData = (props) => (
  <div className={`${styles['c-null']} ${props.className}`}>
    <img src={props.src} />
    <div>{props.text}</div>
  </div>
)

NoData.defaultProps = {
  text: '暂无数据',
  src: 'https://global.uban360.com/sfs/file?digest=fida48a82edf073fa73b7ed188926de0e3f&fileType=2',
  // src: 'https://global.uban360.com/sfs/file?digest=fidbb96f70ef814fbd0b93150e4b9a5382e&fileType=2',
}

export default NoData
