# xm-charts-react

## install

```javascript
npm  install -S xm-charts-react

```

## 用法

```javascript
import { Chart, BarChart, PieChart, LineChart, AreaChart, registerTheme } from 'xm-charts-react'

import * as itemDefaultConfig from 'xm-charts-react/dist/es/itemDefaultConfig'
```

### component props

- **`config`** (required, object)

the echarts option config, can see [http://echarts.baidu.com/option.html#title](http://echarts.baidu.com/option.html#title).

- **`notMerge`** (optional, object)

when `setOption`, merge the data, default is `true`. See [http://echarts.baidu.com/api.html#echartsInstance.setOption](http://echarts.baidu.com/api.html#echartsInstance.setOption).

- **`lazyUpdate`** (optional, object)

when `setOption`, lazy update the data, default is `false`. See [http://echarts.baidu.com/api.html#echartsInstance.setOption](http://echarts.baidu.com/api.html#echartsInstance.setOption).

- **`style`** (optional, object)

the `style` of echarts div. `object`, default is {height: '300px'}.

- **`className`** (optional, string)

the `class` of echarts div. you can setting the css style of charts by class name.

- **`theme`** (optional, string)

the `theme` of echarts. `string`, should `registerTheme` before use it (theme object format: [https://github.com/ecomfe/echarts/blob/master/theme/dark.js](https://github.com/ecomfe/echarts/blob/master/theme/dark.js)). e.g.

```js
// import echarts
import echarts from 'echarts';
...
// register theme object
echarts.registerTheme('my_theme', {
  backgroundColor: '#f4cccc'
});
...
// render the echarts use option `theme`
<LineChart
  config={this.getOption()}
  style={{height: '300px', width: '100%'}}
  className='echarts-for-echarts'
  theme='my_theme' />
```

- **`onChartReady`** (optional, function)

when the chart is ready, will callback the function with the `echarts object` as it's paramter.

- **`loadingOption`** (optional, object)

the echarts loading option config, can see [http://echarts.baidu.com/api.html#echartsInstance.showLoading](http://echarts.baidu.com/api.html#echartsInstance.showLoading).

- **`showLoading`** (optional, bool, default: false)

`bool`, when the chart is rendering, show the loading mask.

- **`onEvents`** (optional, object(string:function) )

binding the echarts event, will callback with the `echarts event object`, and `the echart object` as it's paramters. e.g:

```js
let onEvents = {
  'click': this.onChartClick,
  'legendselectchanged': this.onChartLegendselectchanged
}
...
<LineChart
  config={this.getOption()}
  style={{height: '300px', width: '100%'}}
  onEvents={onEvents} />
```

for more event key name, see: [http://echarts.baidu.com/api.html#events](http://echarts.baidu.com/api.html#events)

- **`opts`** (optional, object)

the `opts` of echarts. `object`, will be used when initial echarts instance by `echarts.init`. Document [here](http://echarts.baidu.com/api.html#echarts.init).

```js
<LineChart
  config={this.getOption()}
  style={{ height: '300px' }}
  opts={{ renderer: 'svg' }} // use svg to render the chart.
/>
```

### Component API & Echarts API

the Component only has `one API` named `getEchartsInstance`.

- **`getEchartsInstance()`** : get the echarts instance object, then you can use any `API of echarts`.

for example:

```js
// render the echarts component below with rel
;<LineChart
  ref={(e) => {
    this.echarts_react = e
  }}
  config={this.getOption()}
/>

// then get the `ReactEcharts` use this.echarts_react

let echarts_instance = this.echarts_react.getEchartsInstance()
// then you can use any API of echarts.
let base64 = echarts_instance.getDataURL()
```

**About API of echarts, can see** [http://echarts.baidu.com/api.html#echartsInstance](http://echarts.baidu.com/api.html#echartsInstance).

You can use the API to do:

1. `binding / unbinding` event.
2. `dynamic charts` with dynamic data.
3. get the echarts dom / dataURL / base64, save the chart to png.
4. `release` the charts.
