import React, { PureComponent } from 'react'

import PieChart from './index'
import { pieEmphasisLabel } from '../itemDefaultConfig'

let index = 0 // 高亮索引

function summate(data) {
  return data.reduce((a, b) => a + b.value * 1, 0)
}

class PieChartDemo extends PureComponent {
  componentDidMount() {
    const pieChart = this.getEchartsInstance()
    pieChart &&
      pieChart.dispatchAction({
        type: 'highlight',
        seriesIndex: 0,
        dataIndex: 0,
      })
  }

  handleMouseOver = (e) => {
    if (e.dataIndex != index) {
      this.getEchartsInstance().dispatchAction({
        type: 'downplay',
        seriesIndex: 0,
        dataIndex: index,
      })
    } else {
      this.getEchartsInstance().dispatchAction({
        type: 'downplay',
        seriesIndex: 0,
        dataIndex: 0,
      })
    }
  }

  handleMouseOut = (e) => {
    index = e.dataIndex
    this.getEchartsInstance().dispatchAction({
      type: 'highlight',
      seriesIndex: 0,
      dataIndex: e.dataIndex,
    })
  }

  getOption() {
    const {
      config: { series },
    } = this.props

    return {
      legend: {
        top: 'center',
        type: 'scroll',
        orient: 'vertical',
        right: '5%',
        itemGap: 15,
        // selectedMode: false,
        icon: 'pin',
        textStyle: {
          fontSize: 14,
          rich: {
            name: {
              color: '#262A30',
            },
            value: {
              padding: [0, 5, 0, 5],
            },
            percent: {
              color: '#00A9FF',
            },
          },
        },

        formatter(name) {
          const total = summate(series[0].data)
          const res = series[0].data.filter((v) => v.name === name)
          const percent = ((res[0].value * 100) / total).toFixed(2)
          return `{name| ${name}} {value|${res[0].value}} {percent|${percent}%}`
        },
      },

      series: (series || []).map((item) => ({
        type: 'pie',
        label: { show: false, position: 'center' },
        emphasis: {
          label: {
            ...pieEmphasisLabel,
          },
        },
        ...item,
      })),
    }
  }

  getEchartsInstance() {
    if (!this.chartRef) return

    return this.chartRef.getEchartsInstance()
  }

  saveRef = (node) => {
    this.chartRef = node
  }

  render() {
    return (
      <PieChart
        ref={this.saveRef}
        option={this.getOption()}
        onEvents={{ mouseover: this.handleMouseOver, mouseout: this.handleMouseOut }}
      />
    )
  }
}

PieChartDemo.defaultProps = {
  config: {
    series: [
      {
        radius: ['30%', '40%'],
        center: ['25%', '50%'],
        data: [
          {
            name: '示意1',
            value: 200,
          },
          {
            name: '示意2',
            value: 600,
          },
        ],
      },
    ],
  },
}

export default PieChartDemo
